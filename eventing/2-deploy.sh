#!/bin/bash

kubectl apply -f sample-app.yaml
kubectl get ns knative-samples --show-labels
kubectl --namespace knative-samples get deployments python-eventing
kubectl --namespace knative-samples get svc python-eventing
kubectl -n knative-samples get trigger python-eventing
kubectl apply -f event-display.yaml
