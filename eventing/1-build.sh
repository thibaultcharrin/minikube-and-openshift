#!/bin/bash

# Build and push the container on your local machine.
docker buildx build --platform linux/amd64 -t "thibaultcharrin/python-eventing" --push .