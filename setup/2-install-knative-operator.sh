#!/bin/bash

if [[ $(kubectl get deployment knative-operator --no-headers=true | wc -l) == 0 ]]; then
    kubectl create -f https://github.com/knative/operator/releases/download/knative-v1.5.1/operator-post-install.yaml
    kubectl apply -f https://github.com/knative/operator/releases/download/knative-v1.11.8/operator.yaml
fi

kubectl get deployment knative-operator
