#!/bin/bash

kubectl apply -f serving.yaml -f kourier.yaml
kubectl --namespace knative-serving get service kourier
kubectl get deployment -n knative-serving
