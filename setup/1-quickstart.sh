#!/bin/bash

if [[ $(kubectl config get-contexts --no-headers=true | wc -l) == 0 ]]; then
    minikube start
fi
if [[ $(kubectl get crd | grep -c operator) == 0 ]]; then
    kubectl create -f https://raw.githubusercontent.com/operator-framework/operator-lifecycle-manager/master/deploy/upstream/quickstart/crds.yaml
    kubectl create -f https://raw.githubusercontent.com/operator-framework/operator-lifecycle-manager/master/deploy/upstream/quickstart/olm.yaml
fi
minikube addons enable ingress
minikube addons enable ingress-dns
kubectl apply -f openshift-console.yaml

# Resources:
# https://github.com/operator-framework/operator-lifecycle-manager/blob/master/doc/install/install.md